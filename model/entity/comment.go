package entity

type CommentCard struct {
	CommentID       int    `db:"id"`
	InterviewID     int    `db:"interview_id"`
	Description     string `db:"description"`
	CreatedBy       string `db:"created_by"`
	CreatedDateTime string `db:"created_datetime"`
	UpdatedBy       string `db:"updated_by"`
	UpdatedDateTime string `db:"updated_datetime"`
}
