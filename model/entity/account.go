package entity

type Account struct {
	AccountID       int    `db:"id"`
	FirstName       string `db:"first_name"`
	LastName        string `db:"last_name"`
	AccountCode     string `db:"account_code"`
	Email           string `db:"email"`
	CreatedBy       string `db:"created_by"`
	CreatedDateTime string `db:"created_datetime"`
	UpdatedBy       string `db:"updated_by"`
	UpdatedDateTime string `db:"updated_datetime"`
}
