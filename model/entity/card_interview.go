package entity

type CardInterview struct {
	InterviewID     int    `db:"id"`
	Name            string `db:"name"`
	Description     string `db:"description"`
	Status          string `db:"status"`
	RecordStatus    string `db:"record_status"`
	CreatedBy       string `db:"created_by"`
	CreatedDateTime string `db:"created_datetime"`
	UpdatedBy       string `db:"updated_by"`
	UpdatedDateTime string `db:"updated_datetime"`
}
