package request

type NewCommentRequest struct {
	InterviewID int    `json:"interview_id"`
	Description string `json:"description"`
	CreatedBy   string `json:"created_by"`
}
