package request

type SaveCardInterviewRequest struct {
	InterviewID  int    `json:"id"`
	RecordStatus string `json:"record_status"`
	UpdatedBy    string `json:"updated_by"`
}
