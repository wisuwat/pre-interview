package request

type UpdateStatusCardInterviewRequest struct {
	InterviewID int    `json:"id"`
	Status      string `json:"status"`
	UpdatedBy   string `json:"updated_by"`
}
