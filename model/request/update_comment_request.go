package request

type UpdateCommentRequest struct {
	CommentID   int    `json:"id"`
	Description string `json:"description"`
	UpdatedBy   string `json:"updated_by"`
}
