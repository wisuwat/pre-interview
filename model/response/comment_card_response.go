package response

type CommentResponse struct {
	CommentID       int    `db:"id"`
	Description     string `db:"description"`
	CreatedBy       string `db:"created_by"`
	CreatedDatetime string `db:"created_datetime"`
}
