package response

type CardIntervieDetailwResponse struct {
	InterviewID     int               `json:"id"`
	Name            string            `json:"name"`
	Description     string            `json:"description"`
	Status          string            `json:"status"`
	CreatedBy       string            `json:"created_by"`
	CreatedDatetime string            `json:"created_datetime"`
	Comment         []CommentResponse `json:"comment"`
}
