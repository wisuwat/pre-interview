package response

type AccountResponse struct {
	AccountId   int    `json:"account_id"`
	FirstName   string `json:"first_name"`
	LastName    string `json:"last_name"`
	AccountCode string `json:"account_code"`
	Email       string `json:"email"`
}
