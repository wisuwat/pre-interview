package repository

import (
	"errors"
	"pre-interview/model/entity"

	"github.com/jmoiron/sqlx"
)

type commentCardRepository struct {
	db *sqlx.DB
}

func NewCommentCardRepository(db *sqlx.DB) commentCardRepository {
	return commentCardRepository{db: db}
}

func (r commentCardRepository) NewComment(commentCard entity.CommentCard) (*entity.CommentCard, error) {
	query := "insert into comments (interview_id, description, created_by, created_datetime, updated_by, updated_datetime) values (?, ?, ?, ?, ?, ?) "
	result, err := r.db.Exec(
		query,
		commentCard.InterviewID,
		commentCard.Description,
		commentCard.CreatedBy,
		commentCard.CreatedDateTime,
		commentCard.UpdatedBy,
		commentCard.UpdatedDateTime,
	)

	if err != nil {
		return nil, err
	}

	id, err := result.LastInsertId()
	if err != nil {
		return nil, err
	}

	commentCard.CommentID = int(id)

	return &commentCard, nil
}

func (r commentCardRepository) UpdateComment(id int, description string, updatedBy string, updatedDatetime string) error {
	query := "update interviews set description=?, updated_by=?, updated_datetime=? where id=? "
	result, err := r.db.Exec(query, description, updatedBy, updatedDatetime, id)
	if err != nil {
		return err
	}

	affected, err := result.RowsAffected()
	if err != nil {
		return err
	}

	if affected <= 0 {
		return errors.New("cannot insert")
	}
	return nil
}
