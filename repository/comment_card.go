package repository

import "pre-interview/model/entity"

type CommentCardRepository interface {
	NewComment(entity.CommentCard) (*entity.CommentCard, error)
	UpdateComment(int, string, string, string) error
}
