package repository

import "pre-interview/model/entity"

type AccountRepository interface {
	GetAll() ([]entity.Account, error)
	GetById(int) (*entity.Account, error)
	Create(entity.Account) (*entity.Account, error)
}
