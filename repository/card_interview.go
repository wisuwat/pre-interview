package repository

import "pre-interview/model/entity"

type CardInterviewRepository interface {
	GetAll() ([]entity.CardInterview, error)
	GetDetailById(int) (*entity.CardInterview, error)
	GetCommentById(int) ([]entity.CommentCard, error)
	NewCard(entity.CardInterview) (*entity.CardInterview, error)
	UpdateStatus(int, string, string, string) error
	SaveCard(int, string, string, string) error
}
