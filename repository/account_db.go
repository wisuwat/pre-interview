package repository

import (
	"pre-interview/model/entity"

	"github.com/jmoiron/sqlx"
)

type accountRepository struct {
	db *sqlx.DB
}

func NewAccountRepository(db *sqlx.DB) accountRepository {
	return accountRepository{db: db}
}

func (r accountRepository) GetAll() ([]entity.Account, error) {
	accounts := []entity.Account{}
	query := "select id, first_name, last_name, email, account_code, created_by, created_datetime, updated_by, updated_datetime from accounts"
	err := r.db.Select(&accounts, query)
	if err != nil {
		return nil, err
	}
	return accounts, nil
}

func (r accountRepository) GetById(id int) (*entity.Account, error) {
	account := entity.Account{}
	query := "select id, first_name, last_name, email, account_code, created_by, created_datetime, updated_by, updated_datetime from accounts where id=? "
	err := r.db.Get(&account, query, id)
	if err != nil {
		return nil, err
	}
	return &account, nil
}

func (r accountRepository) Create(acc entity.Account) (*entity.Account, error) {
	query := "insert into accounts (first_name, last_name, email, account_code, created_by, created_datetime, updated_by, updated_datetime) values (?, ?, ?, ?, ?, ?, ?, ?) "
	result, err := r.db.Exec(
		query,
		acc.FirstName,
		acc.LastName,
		acc.Email,
		acc.AccountCode,
		acc.CreatedBy,
		acc.CreatedDateTime,
		acc.UpdatedBy,
		acc.UpdatedDateTime,
	)

	if err != nil {
		return nil, err
	}

	id, err := result.LastInsertId()
	if err != nil {
		return nil, err
	}

	acc.AccountID = int(id)

	return &acc, nil
}
