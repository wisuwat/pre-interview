package repository

import (
	"errors"
	"pre-interview/model/entity"

	"github.com/jmoiron/sqlx"
)

type cardInterviewRepository struct {
	db *sqlx.DB
}

func NewCardInterviewRepository(db *sqlx.DB) cardInterviewRepository {
	return cardInterviewRepository{db: db}
}

func (r cardInterviewRepository) GetAll() ([]entity.CardInterview, error) {
	cardInterviews := []entity.CardInterview{}

	query := "select id, name, description, status, record_status, created_by, created_datetime, updated_by, updated_datetime from interviews where record_status='A' "
	err := r.db.Select(&cardInterviews, query)
	if err != nil {
		return nil, err
	}
	return cardInterviews, nil
}

func (r cardInterviewRepository) GetDetailById(id int) (*entity.CardInterview, error) {
	cardInterview := entity.CardInterview{}

	query := "select id, name, description, status, record_status, created_by, created_datetime, updated_by, updated_datetime from interviews where id=? and record_status='A' "
	err := r.db.Get(&cardInterview, query, id)
	if err != nil {
		return nil, err
	}
	return &cardInterview, nil
}

func (r cardInterviewRepository) GetCommentById(id int) ([]entity.CommentCard, error) {
	commentCard := []entity.CommentCard{}

	query := "select id, interview_id, description, created_by, created_datetime, updated_by, updated_datetime from comments where interview_id=? "
	err := r.db.Select(&commentCard, query, id)
	if err != nil {
		return nil, err
	}
	return commentCard, nil
}

func (r cardInterviewRepository) NewCard(cardInterview entity.CardInterview) (*entity.CardInterview, error) {
	query := "insert into interviews (name, description, status, record_status, created_by, created_datetime, updated_by, updated_datetime) values (?, ?, ?, ?, ?, ?, ?, ?) "
	result, err := r.db.Exec(
		query,
		cardInterview.Name,
		cardInterview.Description,
		cardInterview.Status,
		cardInterview.RecordStatus,
		cardInterview.CreatedBy,
		cardInterview.CreatedDateTime,
		cardInterview.UpdatedBy,
		cardInterview.UpdatedDateTime,
	)

	if err != nil {
		return nil, err
	}

	id, err := result.LastInsertId()
	if err != nil {
		return nil, err
	}

	cardInterview.InterviewID = int(id)

	return &cardInterview, nil
}

func (r cardInterviewRepository) UpdateStatus(id int, status string, updatedBy string, updatedDatetime string) error {
	query := "update interviews set status=?, updated_by=?, updated_datetime=? where id=? "
	result, err := r.db.Exec(query, status, updatedBy, updatedDatetime, id)
	if err != nil {
		return err
	}

	affected, err := result.RowsAffected()
	if err != nil {
		return err
	}

	if affected <= 0 {
		return errors.New("cannot insert")
	}
	return nil
}

func (r cardInterviewRepository) SaveCard(id int, recordStatus string, updatedBy string, updatedDatetime string) error {
	query := "update interviews set record_status=?, updated_by=?, updated_datetime=? where id=? "
	result, err := r.db.Exec(query, recordStatus, updatedBy, updatedDatetime, id)
	if err != nil {
		return err
	}

	affected, err := result.RowsAffected()
	if err != nil {
		return err
	}

	if affected <= 0 {
		return errors.New("cannot insert")
	}
	return nil
}
