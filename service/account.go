package service

import (
	"pre-interview/model/request"
	"pre-interview/model/response"
)

type AccountService interface {
	GetAccounts() ([]response.AccountResponse, error)
	GetAccount(int) (*response.AccountResponse, error)
	NewAccount(request.NewAccountRequest) (*response.AccountResponse, error)
}
