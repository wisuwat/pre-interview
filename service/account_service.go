package service

import (
	"database/sql"
	"pre-interview/errs"
	"pre-interview/logs"
	"pre-interview/model/entity"
	"pre-interview/model/request"
	"pre-interview/model/response"
	"pre-interview/repository"
	"time"
)

type accountService struct {
	accRepo repository.AccountRepository
}

func NewAccountService(accRepo repository.AccountRepository) accountService {
	return accountService{accRepo: accRepo}
}

func (s accountService) GetAccounts() ([]response.AccountResponse, error) {
	accounts, err := s.accRepo.GetAll()
	if err != nil {
		logs.Error(err)
		return nil, errs.NewUnexpectedError()
	}

	accResponses := []response.AccountResponse{}
	for _, account := range accounts {
		accResponse := response.AccountResponse{
			AccountId:   account.AccountID,
			FirstName:   account.FirstName,
			LastName:    account.LastName,
			AccountCode: account.AccountCode,
			Email:       account.Email,
		}
		accResponses = append(accResponses, accResponse)
	}

	return accResponses, nil
}

func (s accountService) GetAccount(id int) (*response.AccountResponse, error) {
	account, err := s.accRepo.GetById(id)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, errs.NewNotFoundError("account not found")
		}
		logs.Error(err)
		return nil, errs.NewUnexpectedError()
	}

	accResponse := response.AccountResponse{
		AccountId:   account.AccountID,
		FirstName:   account.FirstName,
		LastName:    account.LastName,
		AccountCode: account.AccountCode,
		Email:       account.Email,
	}
	return &accResponse, nil
}

func (s accountService) NewAccount(request request.NewAccountRequest) (*response.AccountResponse, error) {
	account := entity.Account{
		FirstName:       request.FirstName,
		LastName:        request.LastName,
		Email:           request.Email,
		AccountCode:     "ACC" + time.Now().Format("200612150405"),
		CreatedBy:       request.CreatedBy,
		CreatedDateTime: time.Now().Format("2006-1-2 15:04:05"),
		UpdatedBy:       request.CreatedBy,
		UpdatedDateTime: time.Now().Format("2006-1-2 15:04:05"),
	}

	newAcc, err := s.accRepo.Create(account)

	if err != nil {
		logs.Error(err)
		return nil, errs.NewUnexpectedError()
	}

	response := response.AccountResponse{
		AccountId:   newAcc.AccountID,
		FirstName:   newAcc.FirstName,
		LastName:    newAcc.LastName,
		AccountCode: newAcc.AccountCode,
		Email:       newAcc.Email,
	}

	return &response, nil
}
