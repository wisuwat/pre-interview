package service

import (
	"pre-interview/model/request"
	"pre-interview/model/response"
)

type CardInterviewService interface {
	GetCardInterviews() ([]response.CardInterviewResponse, error)
	GetDetailCardInterview(int) (*response.CardIntervieDetailwResponse, error)
	NewCard(request.NewCardInterviewRequest) (*response.CardInterviewResponse, error)
	UpdateStatus(request.UpdateStatusCardInterviewRequest) error
	SaveCard(request.SaveCardInterviewRequest) error
}
