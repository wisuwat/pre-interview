package service

import (
	"pre-interview/model/request"
	"pre-interview/model/response"
)

type CommentCardService interface {
	NewComment(request.NewCommentRequest) (*response.CommentResponse, error)
	UpdateComment(request.UpdateCommentRequest) error
}
