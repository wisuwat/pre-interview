package service

import (
	"database/sql"
	"pre-interview/errs"
	"pre-interview/logs"
	"pre-interview/model/entity"
	"pre-interview/model/request"
	"pre-interview/model/response"
	"pre-interview/repository"
	"time"
)

type cardInterviewService struct {
	cardInterviewRepo repository.CardInterviewRepository
}

func NewCardInterviewService(cardInterviewRepo repository.CardInterviewRepository) cardInterviewService {
	return cardInterviewService{cardInterviewRepo: cardInterviewRepo}
}

func (s cardInterviewService) GetCardInterviews() ([]response.CardInterviewResponse, error) {
	cardInterviews, err := s.cardInterviewRepo.GetAll()
	if err != nil {
		logs.Error(err)
		return nil, errs.NewUnexpectedError()
	}

	cardInterviewResponses := []response.CardInterviewResponse{}
	for _, cardInterview := range cardInterviews {
		cardInterviewResponse := response.CardInterviewResponse{
			InterviewID:     cardInterview.InterviewID,
			Name:            cardInterview.Name,
			Description:     cardInterview.Description,
			Status:          cardInterview.Status,
			CreatedBy:       cardInterview.CreatedBy,
			CreatedDatetime: cardInterview.CreatedDateTime,
		}
		cardInterviewResponses = append(cardInterviewResponses, cardInterviewResponse)
	}

	return cardInterviewResponses, nil
}

func (s cardInterviewService) GetDetailCardInterview(id int) (*response.CardIntervieDetailwResponse, error) {
	cardInterview, err := s.cardInterviewRepo.GetDetailById(id)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, errs.NewNotFoundError("card interview not found")
		}
		logs.Error(err)
		return nil, errs.NewUnexpectedError()
	}

	comments, err := s.cardInterviewRepo.GetCommentById(id)
	if err != nil {
		logs.Error(err)
		return nil, errs.NewUnexpectedError()
	}

	commentResponses := []response.CommentResponse{}
	for _, comment := range comments {
		commentResponse := response.CommentResponse{
			CommentID:       comment.CommentID,
			Description:     comment.Description,
			CreatedBy:       comment.CreatedBy,
			CreatedDatetime: comment.CreatedDateTime,
		}
		commentResponses = append(commentResponses, commentResponse)
	}

	cardInterviewResponse := response.CardIntervieDetailwResponse{
		InterviewID:     cardInterview.InterviewID,
		Name:            cardInterview.Name,
		Description:     cardInterview.Description,
		Status:          cardInterview.Status,
		CreatedBy:       cardInterview.CreatedBy,
		CreatedDatetime: cardInterview.CreatedDateTime,
		Comment:         commentResponses,
	}
	return &cardInterviewResponse, nil
}

func (s cardInterviewService) NewCard(request request.NewCardInterviewRequest) (*response.CardInterviewResponse, error) {
	cardInterview := entity.CardInterview{
		Name:            request.Name,
		Description:     request.Description,
		Status:          "T",
		RecordStatus:    "A",
		CreatedBy:       request.CreatedBy,
		CreatedDateTime: time.Now().Format("2006-1-2 15:04:05"),
		UpdatedBy:       request.CreatedBy,
		UpdatedDateTime: time.Now().Format("2006-1-2 15:04:05"),
	}

	newCard, err := s.cardInterviewRepo.NewCard(cardInterview)

	if err != nil {
		logs.Error(err)
		return nil, errs.NewUnexpectedError()
	}

	response := response.CardInterviewResponse{
		InterviewID:     newCard.InterviewID,
		Name:            newCard.Name,
		Description:     newCard.Description,
		Status:          newCard.Status,
		CreatedBy:       newCard.CreatedBy,
		CreatedDatetime: newCard.CreatedDateTime,
	}

	return &response, nil
}

func (s cardInterviewService) UpdateStatus(request request.UpdateStatusCardInterviewRequest) error {
	err := s.cardInterviewRepo.UpdateStatus(request.InterviewID, request.Status, request.UpdatedBy, time.Now().Format("2006-1-2 15:04:05"))

	if err != nil {
		logs.Error(err)
		return errs.NewUnexpectedError()
	}

	return nil
}

func (s cardInterviewService) SaveCard(request request.SaveCardInterviewRequest) error {
	err := s.cardInterviewRepo.UpdateStatus(request.InterviewID, request.RecordStatus, request.UpdatedBy, time.Now().Format("2006-1-2 15:04:05"))

	if err != nil {
		logs.Error(err)
		return errs.NewUnexpectedError()
	}

	return nil
}
