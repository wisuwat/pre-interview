package service

import (
	"pre-interview/errs"
	"pre-interview/logs"
	"pre-interview/model/entity"
	"pre-interview/model/request"
	"pre-interview/model/response"
	"pre-interview/repository"
	"time"
)

type commentCardService struct {
	commentRepo repository.CommentCardRepository
}

func NewCommentCardService(commentRepo repository.CommentCardRepository) commentCardService {
	return commentCardService{commentRepo: commentRepo}
}

func (s commentCardService) NewComment(request request.NewCommentRequest) (*response.CommentResponse, error) {
	commentCard := entity.CommentCard{
		InterviewID:     request.InterviewID,
		Description:     request.Description,
		CreatedBy:       request.CreatedBy,
		CreatedDateTime: time.Now().Format("2006-1-2 15:04:05"),
		UpdatedBy:       request.CreatedBy,
		UpdatedDateTime: time.Now().Format("2006-1-2 15:04:05"),
	}

	newCommentCard, err := s.commentRepo.NewComment(commentCard)

	if err != nil {
		logs.Error(err)
		return nil, errs.NewUnexpectedError()
	}

	response := response.CommentResponse{
		CommentID:       newCommentCard.CommentID,
		Description:     newCommentCard.Description,
		CreatedBy:       newCommentCard.CreatedBy,
		CreatedDatetime: newCommentCard.CreatedDateTime,
	}

	return &response, nil
}

func (s commentCardService) UpdateComment(request request.UpdateCommentRequest) error {
	err := s.commentRepo.UpdateComment(request.CommentID, request.Description, request.UpdatedBy, time.Now().Format("2006-1-2 15:04:05"))

	if err != nil {
		logs.Error(err)
		return errs.NewUnexpectedError()
	}

	return nil
}
