create database interview;
use interview;

CREATE TABLE interview.accounts (
	id INT auto_increment primary key NOT NULL,
	first_name varchar(255) NULL,
	last_name varchar(255) NULL,
	account_code varchar(100) NULL,
	email varchar(100) NULL,
	created_by varchar(255) NULL,
	created_datetime DATETIME NULL,
	updated_by varchar(255) NULL,
	updated_datetime DATETIME NULL
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8mb4
COLLATE=utf8mb4_unicode_ci;

CREATE TABLE interview.interviews (
	id INT auto_increment primary key NOT NULL,
	name varchar(255) NULL,
	description varchar(255) NULL,
	status CHAR(1) NULL,
	created_by varchar(100) NULL,
	created_datetime DATETIME NULL,
	updated_by VARCHAR(100) NULL,
	updated_datetime DATETIME NULL,
	record_status CHAR(1) NULL
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8mb4
COLLATE=utf8mb4_unicode_ci;


CREATE TABLE interview.comments (
	id INT auto_increment primary key NOT NULL,
	interview_id INT NULL,
	description varchar(255) NULL,
	created_by VARCHAR(100) NULL,
	created_datetime DATETIME NULL,
	updated_by VARCHAR(100) NULL,
	updated_datetime DATETIME NULL
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8mb4
COLLATE=utf8mb4_unicode_ci;

ALTER TABLE interview.comments ADD CONSTRAINT comment_FK FOREIGN KEY (interview_id) REFERENCES interview.interviews(id);