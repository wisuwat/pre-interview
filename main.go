package main

import (
	"fmt"
	"net/http"
	"pre-interview/handler"
	"pre-interview/logs"
	"pre-interview/repository"
	"pre-interview/service"
	"strings"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
	"github.com/jmoiron/sqlx"
	"github.com/spf13/viper"
)

func main() {

	initTimeZone()
	initConfig()
	db := initDatabase()

	accountRepository := repository.NewAccountRepository(db)
	accountService := service.NewAccountService(accountRepository)
	accountHandler := handler.NewAccountHandler(accountService)

	cardInterviewRepository := repository.NewCardInterviewRepository(db)
	cardInterviewService := service.NewCardInterviewService(cardInterviewRepository)
	cardInterviewHandler := handler.NewCardInterviewHandler(cardInterviewService)

	commentCardRepository := repository.NewCommentCardRepository(db)
	commentCardService := service.NewCommentCardService(commentCardRepository)
	commentCardHandler := handler.NewCommentCardHandler(commentCardService)

	router := mux.NewRouter()

	//Account
	router.HandleFunc("/accounts", accountHandler.GetAccounts).Methods(http.MethodGet)
	router.HandleFunc("/account/{accountId:[0-9]+}", accountHandler.GetAccount).Methods(http.MethodGet)
	router.HandleFunc("/create/account", accountHandler.NewAccount).Methods(http.MethodPost)

	//Card interview
	router.HandleFunc("/cardInterviews", cardInterviewHandler.GetCardInterviews).Methods(http.MethodGet)
	router.HandleFunc("/cardInterview/{cardInterviewId:[0-9]+}", cardInterviewHandler.GetDetailCardInterview).Methods(http.MethodGet)
	router.HandleFunc("/create/cardInterview", cardInterviewHandler.NewCard).Methods(http.MethodPost)
	router.HandleFunc("/update/cardInterview", cardInterviewHandler.UpdateStatus).Methods(http.MethodPut)
	router.HandleFunc("/save/cardInterview", cardInterviewHandler.SaveCard).Methods(http.MethodPut)

	//Comment card
	router.HandleFunc("/create/commentCard", commentCardHandler.NewComment).Methods(http.MethodPost)
	router.HandleFunc("/update/commentCard", commentCardHandler.UpdateComment).Methods(http.MethodPut)

	logs.Info("Interview service started at port " + viper.GetString("app.port"))
	http.ListenAndServe(fmt.Sprintf(":%v", viper.GetInt("app.port")), router)
}

func initConfig() {
	viper.SetConfigName("config")
	viper.SetConfigType("yaml")
	viper.AddConfigPath(".")
	viper.AutomaticEnv()
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))

	err := viper.ReadInConfig()
	if err != nil {
		panic(err)
	}
}

func initTimeZone() {
	ict, err := time.LoadLocation("Asia/Bangkok")
	if err != nil {
		panic(err)
	}

	time.Local = ict
}

func initDatabase() *sqlx.DB {
	db, err := sqlx.Open(viper.GetString("db.driver"), fmt.Sprintf("%v:%v@tcp(%v:%v)/%v?parseTime=true",
		viper.GetString("db.username"),
		viper.GetString("db.password"),
		viper.GetString("db.host"),
		viper.GetInt("db.port"),
		viper.GetString("db.database"),
	))
	if err != nil {
		panic(err)
	}

	db.SetConnMaxLifetime(3 * time.Minute)
	db.SetMaxOpenConns(10)
	db.SetMaxIdleConns(10)

	return db
}
