package handler

import (
	"encoding/json"
	"net/http"
	"pre-interview/errs"
	"pre-interview/model/request"
	"pre-interview/service"
)

type commentCardHandler struct {
	commentCardSrv service.CommentCardService
}

func NewCommentCardHandler(commentCardSrv service.CommentCardService) commentCardHandler {
	return commentCardHandler{commentCardSrv: commentCardSrv}
}

func (h commentCardHandler) NewComment(w http.ResponseWriter, r *http.Request) {
	if r.Header.Get("content-type") != "application/json" {
		handleError(w, errs.NewValidationError("request body incorrect format"))
		return
	}

	request := request.NewCommentRequest{}
	err := json.NewDecoder(r.Body).Decode(&request)
	if err != nil {
		handleError(w, errs.NewValidationError("request body incorrect format"))
		return
	}

	response, err := h.commentCardSrv.NewComment(request)
	if err != nil {
		handleError(w, err)
		return
	}

	w.Header().Set("content-type", "application/json")
	w.WriteHeader(http.StatusCreated)
	json.NewEncoder(w).Encode(response)
}

func (h commentCardHandler) UpdateComment(w http.ResponseWriter, r *http.Request) {
	if r.Header.Get("content-type") != "application/json" {
		handleError(w, errs.NewValidationError("request body incorrect format"))
		return
	}

	request := request.UpdateCommentRequest{}
	err := json.NewDecoder(r.Body).Decode(&request)
	if err != nil {
		handleError(w, errs.NewValidationError("request body incorrect format"))
		return
	}

	err = h.commentCardSrv.UpdateComment(request)
	if err != nil {
		handleError(w, err)
		return
	}

	w.Header().Set("content-type", "application/json")
	w.WriteHeader(http.StatusOK)
}
