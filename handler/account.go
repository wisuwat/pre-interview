package handler

import (
	"encoding/json"
	"net/http"
	"pre-interview/errs"
	"pre-interview/model/request"
	"pre-interview/service"
	"strconv"

	"github.com/gorilla/mux"
)

type accountHandler struct {
	accSrv service.AccountService
}

func NewAccountHandler(accSrv service.AccountService) accountHandler {
	return accountHandler{accSrv: accSrv}
}

func (h accountHandler) GetAccounts(w http.ResponseWriter, r *http.Request) {
	accounts, err := h.accSrv.GetAccounts()
	if err != nil {
		handleError(w, err)
		return
	}

	w.Header().Set("content-type", "application/json")
	json.NewEncoder(w).Encode(accounts)
}

func (h accountHandler) GetAccount(w http.ResponseWriter, r *http.Request) {
	accountId, _ := strconv.Atoi(mux.Vars(r)["accountId"])

	account, err := h.accSrv.GetAccount(accountId)
	if err != nil {
		handleError(w, err)
		return
	}

	w.Header().Set("content-type", "application/json")
	json.NewEncoder(w).Encode(account)
}

func (h accountHandler) NewAccount(w http.ResponseWriter, r *http.Request) {
	if r.Header.Get("content-type") != "application/json" {
		handleError(w, errs.NewValidationError("request body incorrect format"))
		return
	}

	request := request.NewAccountRequest{}
	err := json.NewDecoder(r.Body).Decode(&request)
	if err != nil {
		handleError(w, errs.NewValidationError("request body incorrect format"))
		return
	}

	response, err := h.accSrv.NewAccount(request)
	if err != nil {
		handleError(w, err)
		return
	}

	w.Header().Set("content-type", "application/json")
	w.WriteHeader(http.StatusCreated)
	json.NewEncoder(w).Encode(response)
}
