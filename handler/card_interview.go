package handler

import (
	"encoding/json"
	"net/http"
	"pre-interview/errs"
	"pre-interview/model/request"
	"pre-interview/service"
	"strconv"

	"github.com/gorilla/mux"
)

type cardInterviewHandler struct {
	cardInterviewSrv service.CardInterviewService
}

func NewCardInterviewHandler(cardInterviewSrv service.CardInterviewService) cardInterviewHandler {
	return cardInterviewHandler{cardInterviewSrv: cardInterviewSrv}
}

func (h cardInterviewHandler) GetCardInterviews(w http.ResponseWriter, r *http.Request) {
	cardInterviews, err := h.cardInterviewSrv.GetCardInterviews()
	if err != nil {
		handleError(w, err)
		return
	}

	w.Header().Set("content-type", "application/json")
	json.NewEncoder(w).Encode(cardInterviews)
}

func (h cardInterviewHandler) GetDetailCardInterview(w http.ResponseWriter, r *http.Request) {
	cardInterviewId, _ := strconv.Atoi(mux.Vars(r)["cardInterviewId"])

	cardInterview, err := h.cardInterviewSrv.GetDetailCardInterview(cardInterviewId)
	if err != nil {
		handleError(w, err)
		return
	}

	w.Header().Set("content-type", "application/json")
	json.NewEncoder(w).Encode(cardInterview)
}

func (h cardInterviewHandler) NewCard(w http.ResponseWriter, r *http.Request) {
	if r.Header.Get("content-type") != "application/json" {
		handleError(w, errs.NewValidationError("request body incorrect format"))
		return
	}

	request := request.NewCardInterviewRequest{}
	err := json.NewDecoder(r.Body).Decode(&request)
	if err != nil {
		handleError(w, errs.NewValidationError("request body incorrect format"))
		return
	}

	response, err := h.cardInterviewSrv.NewCard(request)
	if err != nil {
		handleError(w, err)
		return
	}

	w.Header().Set("content-type", "application/json")
	w.WriteHeader(http.StatusCreated)
	json.NewEncoder(w).Encode(response)
}

func (h cardInterviewHandler) UpdateStatus(w http.ResponseWriter, r *http.Request) {
	if r.Header.Get("content-type") != "application/json" {
		handleError(w, errs.NewValidationError("request body incorrect format"))
		return
	}

	request := request.UpdateStatusCardInterviewRequest{}
	err := json.NewDecoder(r.Body).Decode(&request)
	if err != nil {
		handleError(w, errs.NewValidationError("request body incorrect format"))
		return
	}

	err = h.cardInterviewSrv.UpdateStatus(request)
	if err != nil {
		handleError(w, err)
		return
	}

	w.Header().Set("content-type", "application/json")
	w.WriteHeader(http.StatusOK)
}

func (h cardInterviewHandler) SaveCard(w http.ResponseWriter, r *http.Request) {
	if r.Header.Get("content-type") != "application/json" {
		handleError(w, errs.NewValidationError("request body incorrect format"))
		return
	}

	request := request.SaveCardInterviewRequest{}
	err := json.NewDecoder(r.Body).Decode(&request)
	if err != nil {
		handleError(w, errs.NewValidationError("request body incorrect format"))
		return
	}

	err = h.cardInterviewSrv.SaveCard(request)
	if err != nil {
		handleError(w, err)
		return
	}

	w.Header().Set("content-type", "application/json")
	w.WriteHeader(http.StatusOK)

}
